(define-module (wmtools wmutils)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 optargs)
  #:use-module (ice-9 textual-ports)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9))

#|
Run a command and return the result in a list of strings.
It might be useful to remove the empty string standard at the end of the list
|#

(define (process-command command)
  (let* ([port (open-pipe command OPEN_READ)]
         [str (get-string-all port)])
    (close-pipe port)
    (delete ""
            (string-split str
                          #\newline))))

(define-syntax run-command
  (syntax-rules ()
    [(_ command)
     (process-command (if (symbol? command) (symbol->string command) command))]

    [(_ command x ...)
     (process-command
      (string-concatenate
       (list
        (if (symbol? command) (symbol->string command) command)
        (string-append " "
                       (cond [(number? x) (number->string x)]
                             [(symbol? x) (symbol->string x)]
                             [else x]))
        ...)))]))

#|
These are some straight implementations of the wmutils commands
|#
(define (option truth . ret)
  (if (or (eqv? truth #f) (eqv? truth 0))
      ""
      (string-concatenate ret)))

;; Fold the list of windows
(define (wfold windows)
  (fold (lambda (x y) (string-concatenate (list x " " y)))
        ""
        windows))

;; Get the exit code for the two annoying af wattr commands
(define (getexitcode window type)
  (status:exit-val
   (close-pipe
    (open-input-pipe
     (string-append "wattr " type
                    (if (number? window)
                        (number->string window)
                        window))))))

(define*-public (chwb window #:optional #:key (color #f) (size 0) (extra-windows '()))
  (run-command 'chwb
               (option color "-c " color)
               (option size "-s " (number->string size))
               (wfold (cons window extra-windows))))

(define*-public (chwso window position)
  (run-command 'chwso
               (cond [(eqv? position 'invert) "-i"]
                     [(eqv? position 'bottom) "-l"]
                     [(eqv? position 'top) "-r"]
                     [else (error "Unsupported type:" position)])
               window))

(define*-public (ignw window type #:optional #:key (extra-windows '()))
  (run-command 'ignw
               (cond [(eqv? type 'set) "-r"]
                     [(eqv? type 'unset) "-s"]
                     [(eqv? type 'toggle) (if (eq? (getexitcode window "o") 0)
                                              "-r"
                                              "-s")])
               (wfold (cons window extra-windows))))

(define*-public (killw window #:optional #:key (kill-application #f) (extra-windows '()))
  (run-command 'killw
               (option kill-application "-p")
               (wfold (cons window extra-windows))))

(define*-public (lsw #:optional #:key (all-windows #f) (root-window #f) (override-redirect #f) (unmapped #f)
                     (windows '()))
  (run-command 'lsw
               (option all-windows "-a")
               (option root-window "-r")
               (option override-redirect "-o")
               (option unmapped "-u")
               (wfold windows)))

(define*-public (mapw window type #:optional #:key (extra-windows '()))
  (run-command 'mapw
               (cond [(eqv? type 'show)   "-m"]
                     [(eqv? type 'toggle) "-t"]
                     [(eqv? type 'hide)   "-u"]
                     [else (error "Unsupport switch:" type)])
               (wfold (cons window extra-windows))))

(define-public (pfw)
  (car (run-command 'pfw)))

(define*-public (wattr attr wid)
  (string-split (car (run-command 'wattr attr wid))
                #\space))

;; I split the functionality of wmp in two since combining them made little sense to me.
(define-public (pattr window)
  (string-split (car (run-command 'wmp window))
                #\space))

(define-public (wmp type x y)
  (run-command 'wmp
               (cond [(eqv? type 'absolute) "-a"]
                     [(eqv? type 'relative) "-r"]
                     [(error "Not a valid switch:" type)])
               "--"
               x
               y))


(define*-public (wmv x y window #:optional #:key (absolute #f) (extra-windows '()))
  (run-command 'wmv
               (option absolute "-a")
               x
               y
               (wfold (cons window extra-windows))))

(define*-public (wrs x y window #:optional #:key (absolute #f) (extra-windows '()))
  (run-command 'wrs
               (option absolute "-a")
               x
               y
               (wfold (cons window extra-windows))))

(define-public (wtf window)
  (run-command 'wtf window))

(define-public (wtp x y width height window)
  (run-command 'wtp x y width height window))

#|
Wmutils opt wrappers
|#
(define-public (wname . windows)
  (delete ""
          (map
           (lambda (window)
             (let ([result (run-command 'wname window)])
               (if (not (null? result))
                   (cons (car result)
                         window)
                   "")))
           windows)))
