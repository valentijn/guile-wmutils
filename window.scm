(define-module (wmtools window)
  #:use-module (oop goops)
  #:use-module (wmtools wmutils)
  #:export (<window> set-size! add-to-size! remove-from-size! set-border-color! get-size
                     get-id get-x-point get-y-point get-width get-height get-override-redirect get-mapped))

#|
Create a simpler interface to deal with the windows
|#

(define-class <window> ()
  (id #:getter get-id
      #:accessor id
      #:init-keyword #:id)

  (border #:getter get-border
          #:accessor border
          #:init-keyword #:border)

  (x-point #:getter get-x-point
           #:accessor x-point)

  (y-point #:getter get-y-point
           #:accessor y-point)

  (width #:getter get-width
         #:accessor width)

  (height #:getter get-height
          #:accessor height)

  (override-redirect #:getter get-override-redirect
                     #:accessor override-redirect)

  (mapped #:getter get-mapped
          #:accessor mapped))

(define-method (initialize (window <window>) initargs)
  (next-method window initargs)
  (update window))

(define-method (update (window <window>))
  (define (set-info border x y width height)
    (slot-set! window 'border border)
    (slot-set! window 'x-point x)
    (slot-set! window 'y-point y)
    (slot-set! window 'width width)
    (slot-set! window 'height height))

  (apply set-info (map string->number (wattr "bxywh" (get-id window)))))

(define-method (= (one <window>) (two <window>))
  (string= (get-id one) (get-id two)))

(define-method (set-size! (width <integer>) (height <integer>)
                          (window <window>))
  (slot-set! window 'width width)
  (slot-set! window 'height height)
  (wrs width height (get-id window) #:absolute #t))

(define-method (add-to-size! (width <integer>) (height  <integer>)
                             (window <window>))
  (slot-set! window 'width (+ (get-width window) width))
  (slot-set! window 'height (+ (get-height window) height))
  (wrs width height (get-id window)))

(define-method (remove-from-size! (width <integer>) (height <integer>)
                                  (window <window>))
  (add-to-size! (- width) (- height) window))

(define-method (get-size (window <window>))
  (cons (get-width window)
        (get-height window)))

(define-method (set-border-color! (window <window>) (red <integer>) (green <integer>) (blue <integer>))
  (chwb (get-id window)
        #:color (string-concatenate (map (lambda (num) (number->string num 16)) (list red green blue)))))

(define-public root-window (make <window> #:id (car (lsw #:root-window #t))))
