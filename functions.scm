(define-module (wmtools functions)
  #:use-module (wmtools wmutils)
  #:use-module (wmtools window)
  #:use-module (oop goops)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-1))

(define (center-in-window window)
  (wmp 'absolute
       (+ (get-x-point window)
          (/ (get-width window) 2))
       (+ (get-y-point window)
          (/ (get-height window) 2))))

(define*-public (tab #:optional (reverse #f))
  (let* ([windows (lsw)]
         [next (member (pfw) windows)]
         [len (length next)])
    ;; Go to the next window unless there is none, in which case go to the first window
    (if reverse
        (cond [(> len 1) (wtf (cadr next))]
              [(= len 1) (wtf (car windows))]
              [else (error "No windows found in:" next)]))))


;; Focus to the specified window and place the cursor in the middle.
(define (focus window)
  (wtf (get-id window))
  (chwso (get-id window) 'top)
  (center-in-window window))

;; Snap the window to place on the window
(define (snap! window position)
  (let ([half-width (/ (get-width root-window) 2)]
        [half-height (/ (get-height root-window) 2)]
        [helper (lambda (x y width height) (begin (slot-set! window 'x-point x)
                                                  (slot-set! window 'y-point y)
                                                  (slot-set! window 'width width)
                                                  (slot-set! window 'height height)
                                                  (wtp x y width height (get-id window))
                                                  (center-in-window window)))])

    (cond [(eqv? position 'topleft) (helper 0 0 half-width half-height)]
          [(eqv? position 'bottomleft) (helper 0 half-height half-width half-height)]
          [(eqv? position 'topright) (helper half-width 0 half-width half-height)]
          [(eqv? position 'bottomright) (helper half-width half-height half-width half-height)]
          [(eqv? position 'left) (helper 0 0 half-width (get-height root-window))]
          [(eqv? position 'right) (helper half-width 0 half-width (get-height root-window))])))
